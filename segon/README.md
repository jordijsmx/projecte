# Projecte ASIX 2024

- <https://gitlab.com/jordijsmx/projecte> 

## Integrants del Grup

- Aleix Ridameya
- Jordi Jubete

## Segon Trimestre

## Administració de sistemes, xarxes i cointainers

En el segon trimestre del projecte d'ASIX tenim com a objectiu el desplegament d'una infraestructura on allotjar un servei web. Com a requisit principal ha de ser accesible mitjançant un navegador o per ordres de terminal des de l'exterior. Fa ús d'una base de dades que no ha de ser accessible des d'Internet.

Es tracta de realitzar les següents implementacions:
- Màquina virtual _Debian_ d'allotjament dels serveis.
- Implementació del servidor _MongoDB._
- Implementació d'un servidor web.
- Implementació del servidor _reverse-proxy_
- Disseny i configuració de tallafocs.
  
El diagrama de la infraestructura serà el següent:

<div align="center">
<img src="imatges/diagrama.png" width="700px" height="400px"/>
</div>

### Instal·lació del SO

Instal·larem una màquina virtual **Debian 12** a **VirtualBox** amb dos discos virtuals en **RAID1** de 20GB cadascún. Tots dos tindran una partició per al sistema de 18GB i una altra amb la resta per **swap**. Per a la creació del dispositiu RAID, el qual realitzarem durant el procés d'instal·lació de Debian, necessitarem prèviament els **dos discos** creats a VirtualBox.

Cadascun dels discos és de 20GB:

<div align="center">
<img src="imatges/discos.png" width="200px" height="100px"/>
</div>


Un cop fets i iniciada l'instal·lació, quan arribem al pas de particionat, haurem de crear manualment el **dispositiu RAID** i assignar-lo a les particions correctes. L'esquema de particions queda, finalment, estructurat de la següent forma:

<div align="center">
<img src="imatges/Capture6.JPG" width="400px" height="200px"/>
</div>

D'aquesta manera, tindrem un dispositiu RAID a on es realitzarà l'instal·lació del **sistema** i un segon dispositiu que s'utilitzarà com a **swap**.

RAID significa **"Redundant Array of Independent Disks"** o conjunt redundant de discos independents. És una tecnología que s'utilitza per combinar varis discos durs en **una sola unitat lògica** amb el fi de millorar el rendiment, la redundància o les dues coses a la vegada.

- RAID0

  RAID0 **divideix les dades** en blocs i les distribueix de forma alternada a través de dos o més discos durs. No ofereix redundància de dades ja que no hi ha duplicació d'informació entre els discos. Hi ha una millora del rendiment al dividir les dades ja que les operacions de lectura i escriptura es poden realitzar de forma simultània.

<div align="center">
<img src="imatges/raid0.jpg" width="200px" height="200px"/>
</div>

- RAID1

  RAID1, en canvi, és una configuració **"mirall"** on les dades s'escriuen simultàniament en dos o més discos durs. Cada disc és una copia exacta de l'altre, el que proporciona redundància completa de dades. Si un disc falla, **les dades permaneixen intactes i accessibles a l'altre disc**, d'aquesta manera tenint la capacitat de funcionar inclòs amb la fallada d'un dels discos. La desavantatge és que ofereix velocitats d'escriptura més reduïdes.

<div align="center">
<img src="imatges/raid1.png" width="200px" height="200px"/>
</div>

### Raonament

Hem escollit RAID1 ja que ens sembla molt més fiable devant de **fallades de discos**. Al tenir una còpia exacta del contingut del disc, augmenta la fiabilitat del sistema. Hi ha menor risc de **pèrdua de dades**, ja que amb RAID0 la pèrdua d'un disc significa la pèrdua de totes les dades. Encara que RAID0 pot oferir un rendiment de lectura lleugerament millor, degut a la distribució de les dades entre els discos, la diferència a la pràctica no es massa significativa. 


### Evidències
- Pantalla del terminal amb les instruccions

```df -h```
```
Filesystem      Size  Used Avail Use% Mounted on
udev            953M     0  953M   0% /dev
tmpfs           197M  1.3M  196M   1% /run
/dev/md0         17G  8.7G  6.9G  56% /
tmpfs           984M     0  984M   0% /dev/shm
tmpfs           5.0M  8.0K  5.0M   1% /run/lock
tmpfs           197M   96K  197M   1% /run/user/1000
```
```cat /proc/mdstat```
```
Personalities : [raid1] [linear] [multipath] [raid0] [raid6] [raid5] [raid4] [raid10] 
md0 : active raid1 sda1[0] sdb1[1]
      17559552 blocks super 1.2 [2/2] [UU]
      
md1 : active raid1 sda5[0] sdb5[1]
      3388416 blocks super 1.2 [2/2] [UU]
      
unused devices: <none>
```
- Verificació de connexió

```ping -cl <ip-guest>```
```
PING 10.200.243.169 (10.200.243.169) 56(84) bytes of data.
64 bytes from 10.200.243.169: icmp_seq=1 ttl=64 time=0.882 ms

--- 10.200.243.169 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.882/0.882/0.882/0.000 ms
```

### MongoDB

MongoDB és un sistema de base de dades NoSQL orientat a documents, de codi obert, escrit en C++, que en comptes d'emmagatzemar l'informació en taules, ho fa en una estructura de dades **BSON** semblant a **JSON**, amb un esquema dinàmic. Al estar escrit en C++, compta amb una notoria capacitat per aproficat els recursos de la màquina i, al estar llicenciat sota **GNU AGPL 3.0**, es possible adaptar-lo a qualsevol necessitat.

L'implementació de MongoDB es realitzarà amb **docker-compose-yml**.

Les característiques més importants establertes són:
- Creació d’un nom de host per a que el container es distingeixi.
- Propagació  del port **27017** per realitzar la connexió.
- Realitzar els volums per tenir persistència de dades i executar el fitxer **data-mongo.js**. Aquest fitxer es fa el volum a la carpeta **/docker-entrypoint-initdb.d/** perquè és una carpeta definida per executar el contingut d'aqueta.
- Realitzarem la creació d'una xarxa per la **comunicació** entre el container de MongoDB i el de Node / Express.
- Utilitzarem un fitxer on estaran emmagatzemades les **variables d'entorn** que necessita el servei per executar-se correctament. 

Variables: 

Les variables que necessita són les que estan definides en el fitxer **data-mongo.js**. Aquestes són:

- MONGO_USERNAME: Nom de l'usuari que es crearà ('guest')
- MONGO_PASSWORD: Contrasenya de l'usuari ('guest')
- MONGO_DATABASE: Nom de la base de dades ('films')

Podriem utilitzar aquestes variables directament en el **docker-compose** però hem decidit afegir-les a un fitxer anomenat **variables.env** per a tenir una millor seguretat. Ja que emmagatzemar informació sensible, en aquest cas, usuari, contrasenya o el nom de la base de dades, directament en arxius de configuració o en codi font augmenta el risc d'exposició accidental.

També pot fer l'arxiu reutilitzable en diferents entorns o projectes.

#### Evidències

- Contingut del fitxer docker-compose.yml

```cat docker-compose.yml```
```
image: mongo:7-jammy
    container_name: mongo.edt.org
    hostname: mongo.edt.org
    ports:
      - "27017:27017"
    volumes: 
      - "/var/lib/mongodb:/var/lib/mongodb"
      - "./data-mongo.js:/docker-entrypoint-initdb.d/data-mongo.js"
    networks:
      - db-network
    env_file:
      - variables.env
```
- Contenidors en execució:

```sudo docker ps```
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                      NAMES
985dd47f4573   mongo:7-jammy           "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   0.0.0.0:27017->27017/tcp, :::27017->27017/tcp                              mongo.edt.org
72bfd7fb7839   fitxers-reverse-proxy   "/docker-entrypoint.…"   44 minutes ago   Up 44 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp   reverse-proxy
ee29856e0361   fitxers-node-app        "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   3000/tcp, 0.0.0.0:5000->5000/tcp, :::5000->5000/tcp                        node-app

```
- Comprovació que el port _MongoDB_ està en escola i accessible:
Des de màquina Guest:

```sudo netstat -a | grep 27017```
```
tcp        0      0 0.0.0.0:27017           0.0.0.0:*               LISTEN     
tcp6       0      0 [::]:27017              [::]:*                  LISTEN     
```

Des de Host:

```nmap <ip_guest>```
```
Starting Nmap 7.93 ( https://nmap.org ) at 2024-03-11 12:10 CET
Initiating Ping Scan at 12:10
Scanning 10.200.243.169 [2 ports]
Completed Ping Scan at 12:10, 0.00s elapsed (1 total hosts)
Initiating Connect Scan at 12:10
Scanning 10.200.243.169 [65535 ports]
Discovered open port 443/tcp on 10.200.243.169
Discovered open port 22/tcp on 10.200.243.169
Discovered open port 80/tcp on 10.200.243.169
Discovered open port 27017/tcp on 10.200.243.169
Discovered open port 5000/tcp on 10.200.243.169
Completed Connect Scan at 12:10, 0.62s elapsed (65535 total ports)
Nmap scan report for 10.200.243.169
Host is up (0.000094s latency).
Not shown: 65530 closed tcp ports (conn-refused)
PORT      STATE SERVICE
22/tcp    open  ssh
27017/tcp open  mongod

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.65 seconds
```
- Accés a MongoDB amb un client qualsevol
  

### Web UI

Per a la visualització de les dades de **MongoDB** s'utilitzara **Node / Express**, són dues tecnologies molt utilitzades en el desenvolupament d'aplicacions web.

**Node.js:** És un entorn que treballa en temps d'execució, de codi obert, multi-plataforma, que permet a desenvolupador crear tota mena d'eines del costat servidor i aplicacions en Javascript. L'execució en temps real està pensada per utilitzar-se fora del context d'un explorador web, és a dir, executar-se directament en un sistema operatiu de servidor. Afegeix suport a les APIs de sistema operatiu més tradicional que inclou HTTP i biblioteques de sistemes de fitxers.

Treballa de forma asíncrona, és a dir ens permet realitzar peticions que no serà necessari que es respongui al moment.

**Express:** És un marc web minimalista i flexible per a Node.js que simplifica el desenvolupament d'aplicacions web i API. Proporciona una capa d'abstracció sobre el servidor HTTP de Node, el qual facilita la creació de rutes, gestió de sol·licituds i respostes, middleware i plantilles de vistes.

Les característiques del **docker compose** són:

- Utilitzar l’opció **build** per dur a terme la creació del container a partir d’una directori on estaran les opcions de creació del container.
- Disposar d’un **nom** de host per a que el container es distingeixi per nom.
- Propagació del port **5000** per la visualització des del navegador.
- Creació de dues xarxes per la comunicació entre **MongoDB** i el **Reverse Proxy**.
- Utilitzarem un fitxer on estaran emmagatzemades les **variables d'entorn** que necessita el fitxer **server.js** per executar-se correctament.

**Variables:**

Les variables que necessita són les que estan definides en el fitxer server.js. Aquestes són:

- MONGO_DATABASE: Nom de la base de dades de mongo (‘films’)
- MONGO_USERNAME: Nom de l’usuari de mongo (‘guest’)
- MONGO_PASSWORD: Contrasenya de l’usuari de mongo (‘guest’)
- MONGO_HOST: Nom del servidor de mongo (‘mongo.edt.org’)
- MONGO_PORT: Port per on es comunica el mongo (‘27017’)

Com abans, desem aquestes variables al fitxer **variables.env** per a una millor seguretat, minimitzant el risc d'exposició accidental.

Característiques del Dockerfile:

- Crear la carpeta on estaran els fitxers importants per a que funcioni la app
- Instal·lem el npm per poder executar el server.js
- Executem el server.js amb  l’ordre node.
- Exposem els ports necessaris per el seu funcionament.

#### Evidències
- Contingut del fitxer docker-compose.yml
  
```cat docker-compose.yml```
```
  node-app:
    build: ./node-app
    container_name: node-app
    ports:
      - "5000:3000"
    networks:
      - web-network
      - db-network
    env_file:
      - variables.env
```
- Contingut del fitxer Dockerfile

```cat Dockerfile```
```
FROM node:20-alpine3.17
RUN mkdir -p /usr/src/app
COPY * /usr/src/app
WORKDIR /usr/src/app
RUN npm install
CMD node server.js
EXPOSE 3000
```
- Contenidors en execució
  
```sudo docker ps```
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                      NAMES
985dd47f4573   mongo:7-jammy           "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   0.0.0.0:27017->27017/tcp, :::27017->27017/tcp                              mongo.edt.org
ee29856e0361   fitxers-node-app        "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   3000/tcp, 0.0.0.0:5000->5000/tcp, :::5000->5000/tcp                        node-app
```
- Accedir a la pàgina web
```localhost:5000```

<div align="center">
<img src="imatges/capturalocalhost.png" width="600px" height="400px"/>
</div>

### Reverse Proxy

Un servidor proxy invers, o reverse proxy, és un tipus de servidor que recupera recursos en nom d'un client extern, desde un o més servidors interns. Aquests recursos es tornen al client com si s'originéssin en el propi servidor Web. Contrariament a un proxy forward, un proxy invers és un intermediari per a que els servidors associats siguin contactats per qualsevol client.

En aquest projecte utilitzem el proxy invers per a la redirecció del port de l'aplicació de **Node / Express**, que es el 3000, i sigui redirigit als ports 80 i 443. Utilitzant el servidor web **Nginx**, serà configurat com a proxy i crearem una clau i certificat autosignats per a la securització del tràfic amb **TLS**.

Ordre de la clau i certificat autosignats
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout projectekey.key -out projectecert.crt
```

Un cop creada, l'hem d'afegir al seu directori, de dins del container, per a que despres pugui ser utilitzada per a Nginx en la redirecció del port 80 al port securitzat 443. Hem de tenir clar que una clau i certificat autosignats **no són segurs** per a un entorn de producció.

### Evidències
- Contingut del fitxer Dockerfile
  
```cat reverse-proxy/Dockerfile```
```
FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
COPY projectekey.key /etc/nginx/projectekey.key
COPY projectecert.crt /etc/nginx/projectecert.crt
EXPOSE 80
EXPOSE 443
```
- Contingut del fitxer nginx.conf

```cat reverse-proxy/nginx.conf```
```
events {
    worker_connections 1024;
}
http {
  server {
    listen 80;
    server_name localhost;
    return 301 https://$server_name$request_uri;
  }
  server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate /etc/nginx/projectecert.crt;
    ssl_certificate_key /etc/nginx/projectekey.key;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_pass http://node-app:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
  }
}
```
- Contenidors en execució

```sudo docker ps```
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                      NAMES
985dd47f4573   mongo:7-jammy           "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   0.0.0.0:27017->27017/tcp, :::27017->27017/tcp                              mongo.edt.org
ee29856e0361   fitxers-node-app        "docker-entrypoint.s…"   44 minutes ago   Up 44 minutes   3000/tcp, 0.0.0.0:5000->5000/tcp, :::5000->5000/tcp                        node-app
72bfd7fb7839   fitxers-reverse-proxy   "/docker-entrypoint.…"   44 minutes ago   Up 44 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp   reverse-proxy
```
- Comprovació dels ports 80 i 443 oberts
Màquina Guest:

```sudo netstat -a | grep http```
```
tcp        0      0 0.0.0.0:https           0.0.0.0:*               LISTEN     
tcp        0      0 0.0.0.0:http            0.0.0.0:*               LISTEN     
tcp        0      0 debian:55820            93.243.107.34.bc.:https ESTABLISHED
tcp        0      0 debian:58682            239.237.117.34.bc:https TIME_WAIT  
tcp6       0      0 [::]:https              [::]:*                  LISTEN     
tcp6       0      0 [::]:http               [::]:*                  LISTEN
```
- Màquina Host

```nmap -p- --open --min-rate=5000 -Pn -v -sS -n <ip_guest>```
```
Starting Nmap 7.93 ( https://nmap.org ) at 2024-03-11 12:10 CET
Initiating Ping Scan at 12:10
Scanning 10.200.243.169 [2 ports]
Completed Ping Scan at 12:10, 0.00s elapsed (1 total hosts)
Initiating Connect Scan at 12:10
Scanning 10.200.243.169 [65535 ports]
Discovered open port 443/tcp on 10.200.243.169
Discovered open port 22/tcp on 10.200.243.169
Discovered open port 80/tcp on 10.200.243.169
Discovered open port 27017/tcp on 10.200.243.169
Discovered open port 5000/tcp on 10.200.243.169
Completed Connect Scan at 12:10, 0.62s elapsed (65535 total ports)
Nmap scan report for 10.200.243.169
Host is up (0.000094s latency).
Not shown: 65530 closed tcp ports (conn-refused)
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
443/tcp   open  https
5000/tcp  open  upnp
27017/tcp open  mongod

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.65 seconds
```

### Tallafocs

La funció d'un tallafocs és protegir una xarxa o sistema informàtic d'accessos **no autoritzats**, filtrant el tràfic i bloquejant o permetent certs tipus de comunicacions. Ens servirà per tancar els accessos a altres serveis de la nostra màquina Guest no desitjats, ja que ara té oberts els ports:

- SSH (22)
- HTTP (80)
- HTTPS (443)
- Nodejs / Express (5000)
- MongoDB (27017)

Aquests ports són accessibles desde fora de la màquina virtual Guest, per tant, configurarem el nostre tallafocs de manera que només siguin accessibles els ports **80 i 443**, ja que només ens interessa el tràfic a través d'aquests ports.

#### Evidències
- Captura de les regles
```
# Neteja de regles i cadenes existents
iptables -F
iptables -X

# Establir política predeterminada a DROP para INPUT, FORWARD y OUTPUT
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

# Permetre connexions entrants per els ports 80 i 443
iptables -A FORWARD -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -j ACCEPT
```
- Llistat regles actives
```
Chain INPUT (policy DROP)
target     prot opt source               destination         

Chain FORWARD (policy DROP)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:http
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:https

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
```

