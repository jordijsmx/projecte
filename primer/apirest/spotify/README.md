# Spotify Web API 

## Introducció 
La pràctica consisteix en realitzar una app que donat un nom d'artista es mostren les 10 cançons més escoltades de l'artista. Per poder desenvolupar la app utilitzarem la API que ens proporciona Spotify.Necessitarem tenir instal·lat el **nodejs** i el **npm**. Per poder-se connectar entre si han d'utilitzar els identificadors de la app de spotify "client ID i client secret" per identificarte. També s'utilizaran els tokens d'autorització, que son unes dades amb un temps límit que s'utilitzen com a autorització per a concedir l'accés a les dades.

### Preparació 

+ Descarregar un model d'exemple que connecta amb la API Rest de Spotify. Aquest exempla esta [aquí](https://github.com/spotify/web-api-examples).

+ Crear una compte de Spotify, accedir a la pàgina web de [Spotify for Developers](https://developer.spotify.com) i crear un app.

### Creació de la app a Spotify

Quan tenim la compte de Spotify creada accedim a la web anomenada anteriorment. Anem al nostre perfil > dashboard i creem la app.

- Premer apartat: CREATE APP
  - App name "lliure"
  - App description "lliure"
  - Website: "res"
  - Redirect URIs: "http://localhost:8888/callback"
  - Which API: "Web API"

### Edició del model d'exemple API Rest
#### Zona de treball local: /spotify/authorization/authorization_code
- Comprovar que app.js connecta amb Spotify: 
    - Editem el fitxer **app.js** on haurem de posar informació de la app creada en spotify, la informació requerida esta en [Spotify for Developers](https://developer.spotify.com) > Dashboard > app creada > settings. La informació s'ha de posar a la variable corresponent al principi del fitxer app.js.
        - Client ID
        - Client secret
        - Redirect URIs
    - Iniciar prova realitzant:
        ```$ node app.js```
    - Si els pasos realitzats estan correctament anar al navegador i cercar http://localhost:8888 i ens hauria de mostrar:
    ![hola](../images/login.png)
    ![hola](../images/logged.jpg)
Tokens:
      - El "Access token" són els que proporcionen l'autorització per concedir l'accés als recursos demanats. Tenen un temps limitat per raons de seguretat, un cop expirats, el client utilitza el "Refresh Token" per obtenir un nou Access token sense tenir que autenticar-nos de nou.

### Muntatge per realizar la finalitat de la pràctica
#### Zona de treball local: /spotify/authorization/authorization_code
Per concluir amb l'objectiu de la pràctica crearem dos carpetas dintre de **public** i cada carpeta contidrà un fitxer que aquests 2 fitxers estaran connectats amb el index.html, el visualitzador de la pàgina.
- scripts
  - spotify.js: Fitxer on es realitzaran les crides.    
- styles
  - spotify.css: Estètica de la pàgina.

- index.html: Visualitzador de la pàgina.


Edició de l'index.html:
- Retallarem el script "el últim del fitxer" i el enganxarem en el fitxer spotify.js.
- Retallarem el contingut de l'etiqueta style i l'enganxarem en el fitxer spotify.css
- Afegirem dos lineas per carregar els estils i el javascript, la carrega del javascript ha d'estar per sota dels altres perquè es important que sigui l'últim en executar-se.
```html
<link rel="stylesheet" type="text/css" href="styles/spotify.css"/>
<script type="application/javascript" src="scripts/spotify.js"></script>
```
- També afagirem un requadre on es podrà indicar el nom de l'artista i dos botons un per cercar els top 10 de l'artista i l'altre per natejar la recerca.
```html
<div id="loggedin">
      <div id="user-profile"></div>
      <div class="input">
         <input id="name">
         <button id="button">¡Get your favourite artist's Top Tracks!</button>
         <button id ="clear_results">Clear results</button>
       </div>
       <dl id="resultats" class="dl-horizontal">
         <dt>Artists</dt>
         <dd id="artists" class="text-overflow"></dd>
         <dt>Top Tracks</dt>
         <dd id="top_tracks" class="text-overflow"></dd>
       </dl>
       <div id="oauth"></div> 
      <button class="btn btn-default" id="obtain-new-token">Obtain new token using the refresh token</button>
    </div>
```

Edició de spotify.js:
- Afegir la funció que dur a terme el clear.
```javascript
$("#clear_results").click(function(){

    $('#top_tracks').empty();
    $('#artists').empty();
```
- Afegir la funció que implantar la petició de l'usuari.

```javascript
//recollim el valor de #name i fem la petició quan l'usuari fa click a button
document.getElementById('button').addEventListener('click', function() {

    //netegem tots els resultats cada cop que fem una nova crida
    $('#artists').empty();
    $('#top_tracks').empty();

    //recollim el valor del input
    var artistName = $("#name").val();

    // comprovem si hi ha espais
    if ($("#name").val().indexOf(" ") != -1){
        var replaceSpace = $.trim($("#name").val());
        artistName = replaceSpace.replace(/ /g, "%20");
    }

    // si no hi ha cap error i existeix acces_token procedim amb la crida
    if (access_token) {

        //crida ajax a /search amb access_token
        $.ajax({
            url: 'https://api.spotify.com/v1/search?q=' + artistName + '&type=artist',
            market: 'ES',
            headers: {
                'Authorization': 'Bearer ' + access_token
            },
            success: function(response) {

                console.log(response);
                //recorrem la resposta la resposta per poder mostrar-la per pantalla

                var artistsArray= response.artists.items;
                var $id;

                $.each(artistsArray, function(key, value){

                    $id = artistsArray[key].id;

                    var $name = artistsArray[key].name;
                    var $nameDiv = $("<div class ='name'/>");
                    var $button = $("<button class='button' id=" + $id + "/>");
                    var $object = $("<div class='object'/>");

                    $nameDiv.append($name);
                    $button.append($nameDiv);
                    $object.append($button);
                    $("#artists").append($object);
                });
                $('.button').click(function(){

                    //segona crida ajax a /top-tracks amb access_token
                    $.ajax({

                        url: 'https://api.spotify.com/v1/artists/'+ this.id +'/top-tracks?country=ES',
                        type:"GET",
                        headers: {
                            'Authorization': 'Bearer ' + access_token,
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                        },

                        success:function(result){
                            console.log(result);

                            $.each(result.tracks, function(key, value){

                                var $top_track = value.name;
                                var $link = $('<a/>').addClass("link").attr("href",value.uri);
                                var $new_button = $("<button class='new_button'/>");
                                var $new_object = $("<div class='new_object'/>");

                                $new_button.append($top_track);
                                $link.append($new_button);
                                $new_object.append($link);
                                $("#top_tracks").append($new_object);

                            });            

                        },

                        error:function(error){
                            console.log(error)
                        }
                    });
                });

            }

        });
    //missatge d'error
    } else{
        alert('There was an error during the authentication');
    }
});
}
})();
```
Edició de spotify.css:
- Afegim el codi necessari per mostra el resultat d'una forma visual mes estètica. Per exemple posant marges:
```css
#login, #loggedin {
  display: none;
}

#loggedin {
  margin-top: 20px;
  background-color: grey;
}

.text-overflow {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: normal;
  width: 500px;
  background: rgb(205,205,205);
  background: radial-gradient(circle, rgba(205,205,205,1) 2%, rgba(101,101,101,1) 100%, rgba(186,186,186,1) 100%); 
  box-shadow: 10px 10px 10px grey;
  border: 10px solid;
}

#user-profile{
  background-color: black;
  padding-left: 20px;
  padding-top: 20px;
}

.media {
  color: white;
}

h1{
  color: lawngreen;
  margin: 0;
}

#resultats{
  display: flex;
  height: 650px;
  width: 90%;
  margin-right: auto;
  padding-bottom: 25px;
}

#artists{
  margin-right: 30px;
}

.object, .new_object{
  margin: 5px;
}

.new_object{
  width: auto;
}

.input{
  margin-top: 30px;
  margin-bottom: 30px;
  text-align: center;
}

#oauth, #obtain-new-token{
  display: none;
}
``` 
### Resultat
#### Zona de treball local: /spotify/authorization/authorization_code
Comprovem que tot estigui guardat en el seu lloc i realitzem l'ordre **node app.js** per activar la web. Anem a http://localhost:8888 i comprovem el funcionament.
![hola](../images/spotify_result.png)